//
//  AppCoordinator.swift
//  AppBook-iOS
//
//  Created by Lucas Leonardo Freitas on 10/02/22.
//

import Foundation
import UIKit

class AppCoordinator{
    
    var window: UIWindow
    var loginCoordinator: LoginCoordinator?
    var viewModel: LoginViewModel?
    var loginViewController: LoginViewController?
    
    required init(window: UIWindow){
        self.window = window
        self.window.makeKeyAndVisible()
    }
    
    func start(){
        loginCoordinator = LoginCoordinator(window: window)
        loginCoordinator?.start()
    }
}
