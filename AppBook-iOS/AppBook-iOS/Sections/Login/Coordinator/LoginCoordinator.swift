//
//  LoginCoordinator.swift
//  AppBook-iOS
//
//  Created by Lucas Leonardo Freitas on 10/02/22.
//

import Foundation
import UIKit

class LoginCoordinator{
    
    var window: UIWindow
    var viewModel: LoginViewModel?
    var loginViewController: LoginViewController?
    var navigationController: UINavigationController?
    
    required init(window: UIWindow){
        self.window = window
    }
    
    func start(){
        viewModel = LoginViewModel()
        loginViewController = LoginViewController()
        
        navigationController = UINavigationController(rootViewController: loginViewController!)
        window.rootViewController = navigationController
    }
}
